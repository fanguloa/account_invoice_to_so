{
    'name': 'Quotation from Invoice',
    'images': ['images/main_screenshot.png'],
    'version': '1.0.2',
    'category': 'Invoice',
    'summary': 'Create a Quotation using invoice lines',
    'author': 'Inovacijos',
    'website': 'http://www.innovations.lt',
    'depends': [
	'sale',
        'account',
    ],
    'installable': True,
    'license': 'AGPL-3',
    'data': [
        'views/views.xml',
    ],
}
