# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning


class Invoice(models.Model):
   _inherit = 'account.invoice'

   @api.multi
   def so_generate(self, invoice_id):
      i = self.env['account.invoice'].search([('id', '=', invoice_id)])
      values = {
        'partner_id': i.partner_id.id,
        'currency_id': i.currency_id.id,
        'company_id': i.company_id.id,
        'amount_untaxed': i.amount_untaxed,
        'amount_tax' : i.amount_tax,
        'amount_total' : i.amount_total,
        'origin' : i.number,
      }
      so = self.env['purchase.order'].create(values)
      if not so:
        raise Warning('Fsiled to create Quotation from Invoice '+i.number)
      lines = self.env['account.invoice.line'].search([('invoice_id', '=', i.id)])
      for line in lines:
        values = {
          'company_id':  line.company_id.id,
          'currency_id': line.currency_id.id,
          'product_uom': line.uom_id.id,
          'product_id': line.product_id.id,
          'invoice_status': 'no',
          'state': 'draft',
          'order_id': so.id,
          'name': line.name,
          'price_unit': line.price_unit,
          'price_subtotal': line.price_subtotal,
          'price_total': line.price_total,
          'discount': line.discount,
          'product_uom_qty': line.quantity,
          'tax_id': [(6, 0, line.invoice_line_tax_ids.ids)],
        }
        self.env['purchase.order.line'].create(values)
      return so.id
